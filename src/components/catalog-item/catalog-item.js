import React from 'react';

import './catalog-item.css';
const CatalogItem = ({ item,onInc }) => {
    return (
        <div className="catalog-item">
            <img src="img/1.jpg" className="catalog-item-image"/>
            <div>
                <h6>{item.title}</h6>
                <span>{item.cost}</span>
                <button className="btn btn-primary"
                    onClick={onInc}>Add</button>
            </div>
        </div>
    )
}

export default CatalogItem;