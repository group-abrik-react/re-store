import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { HomePage, CartPage } from '../pages';
import ShopHeader from '../shop-header';

const App = () => {

    return (
        <main role="main" className="container">
            <ShopHeader numItems={5} total={256} />
            <Switch>
                <Route path="/home" component={HomePage} />
                <Route path="/cart" component={CartPage} />
                <Route path="/" component={HomePage} exact />
            </Switch>
        </main>
    )
}

export default App;