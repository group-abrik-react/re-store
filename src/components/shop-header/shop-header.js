import React from 'react';
import { Link } from 'react-router-dom';
import './shop-header.css'

const ShopHeader = ({ numItems, total }) => {
    return (
        <div className="shop-header">
            <Link to="/">
                <div  className="logo text-dark">ReStore</div>
            </Link>
            <Link to="/cart">
                <div  className="shopping-cart">
                    <i className="cart-icon fas fa-shopping-cart"></i>
                    {numItems} items (${total})
                </div>
            </Link>
        </div>
    )
}

export default ShopHeader;
