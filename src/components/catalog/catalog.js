import React from 'react';
import CatalogItem from '../catalog-item';
import { withBookstoreService, withData } from '../hoc';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';

const Catalog = (props) => {
  const { data,inc} = props;
  if (!data) {
    return null;
  }

  const items = data.map((item) => {
    return (
      <li className="list-group-item"
        key={item.id}>
        <CatalogItem 
          item={item}
          onInc={()=>inc()} />
      </li>
    )
  })

  return (
    <div>
      <ul className="item-list list-group">
        {items}
      </ul>
    </div>
  )
}

const mapMethodsToProps = (bookstoreService) => ({getData: bookstoreService.getBooks});

const mapStateToProps = (state) =>{
  return {
      counter: state
  }
}
const mapDispatchToProps = (dispatch) =>{
  return bindActionCreators(actions,dispatch);    
}

export default connect(mapStateToProps,mapDispatchToProps)(
  withBookstoreService(withData(Catalog), mapMethodsToProps)
);
