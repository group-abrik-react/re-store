import {BookstoreServiceProvider,BookstoreServiceConsumer} from './service-context'
export {
    BookstoreServiceProvider,
    BookstoreServiceConsumer
};