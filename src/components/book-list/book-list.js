import React from 'react';
import BookListItem from '../book-list-item';
import './book-list.css';

import { connect } from 'react-redux';
import { withBookstoreService } from '../hoc';
import { fetchBooks, onIncrease } from '../../actions';
import compose from '../../utils';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator/error-indicator';

const BookList = ({ books, onIncrease }) => {
    return (
        <div>
            <ul className="book-list">
                {
                    books.map((book) => {
                        return (
                            <li key={book.id}>
                                <BookListItem book={book}
                                    onIncrease={onIncrease} />
                            </li>
                        )
                    })
                }

            </ul>
        </div>
    )
};

class BookListContainer extends React.Component {

    componentDidMount() {
        this.props.fetchBooks();
    }

    render() {
        const { books, loading, error, onIncrease } = this.props;

        if (loading) {
            return <Spinner />
        }

        if (error) {
            return <ErrorIndicator />
        }

        return <BookList books={books} onIncrease={onIncrease} />
    }
}



const mapStateToProps = ({ bookList: { books, loading, error } }) => {
    return { books, loading, error }
}
const mapDispatchToprops = (dispatch, { bookstoreService }) => {
    return {
        fetchBooks: fetchBooks(dispatch, bookstoreService),
        onIncrease: (bookId) => {
            dispatch(onIncrease(bookId));
        }
    }
};

export default compose(
    withBookstoreService(),
    connect(mapStateToProps, mapDispatchToprops)
)(BookListContainer);