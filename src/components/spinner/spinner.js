import React from 'react'

import './spinner.css'
const Spinner = () =>{
    return (     
        <div className="spinner">
            <div className="loadingio-spinner-spinner-p4w30um2scf"><div className="ldio-ed7uhzi6nao">
                <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
            </div></div>
        </div>   
    )
}

export default Spinner;