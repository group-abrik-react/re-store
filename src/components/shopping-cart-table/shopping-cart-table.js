import React from 'react';
import { connect } from 'react-redux';
import compose from '../../utils';
import { withBookstoreService } from '../hoc';
import { onIncrease, onDecrease, onDelete } from '../../actions';
import "./shopping-cart-table.css"

const ShoppingCartTable = ({ items, total, onIncrease, onDecrease, onDelete }) => {

    if (items.length == 0) {
        return <div>Корзина пуста</div>;
    }

    return (
        <div className="shopping-cart-table">
            <h2>Your Order</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th>Count</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        items.map((item, idx) => {
                            if (item == undefined) {
                                return null;
                            }
                            return (
                                <tr key={item.id}>
                                    <td>{idx + 1}</td>
                                    <td>{item.title}</td>
                                    <td>{item.count}</td>
                                    <td>{item.total}</td>
                                    <td>
                                        <button className="btn btn-outline-success btn-sm"
                                            onClick={() => { onIncrease(item.id) }}
                                        >
                                            <i className="fa fa-plus-circle"></i>
                                        </button>
                                        <button className="btn btn-outline-warning btn-sm"
                                            onClick={() => { onDecrease(item.id) }}>
                                            <i className="fa fa-minus-circle" aria-hidden="true"></i>
                                        </button>
                                        <button className="btn btn-outline-danger btn-sm"
                                            onClick={() => { onDelete(item.id) }}>
                                            <i className="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            <div className="total">
                Total: ${total}
            </div>
        </div>
    )
}

const mapStateToProps = ({ shopingCart: { items, orderTotal } }) => {
    return {
        items: items,
        total: orderTotal
    }
}
const mapDispatchToProps = { onIncrease, onDecrease, onDelete };

export default compose(
    withBookstoreService(),
    connect(mapStateToProps, mapDispatchToProps)
)(ShoppingCartTable);