import React from 'react';

const OrderItem = ({ item,onInc,onDec }) => {
    return (
        <React.Fragment>
            { item.id} / { item.title} / { item.count} / { item.cost}
            <button className="btn btn-primary btn-sm" 
                onClick={onInc}
                > +</button >
            <button className="btn btn-primary btn-sm"
                onClick={onDec}>-</button>
            <button className="btn btn-primary btn-sm">x</button>
        </React.Fragment>
    )
}

export default OrderItem;