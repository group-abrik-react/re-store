import React from 'react';
import { BookstoreServiceConsumer } from '../service-context';

const withBookstoreService = ()=>(Wrapped/*,mapMethodsToProps*/) => {
  return (props) => {
    return (
      <BookstoreServiceConsumer>
        {
          (bookstoreService) => {
            /*const serviceProps = mapMethodsToProps(bookstoreService);*/

            return (
              <Wrapped {...props} bookstoreService={bookstoreService}/*{...serviceProps} */
              />
            )
          }
        }
      </BookstoreServiceConsumer>
    )
  }
}

export default withBookstoreService;