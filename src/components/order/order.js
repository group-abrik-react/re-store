import React from 'react';
import OrderItem from '../order-item';
import { withBookstoreService, withData } from '../hoc';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';

const Order = (props) => {
  const { data,inc,dec,counter } = props;
  console.log(props);

  if (!data) {
    return null;
  }

  const items = data.map((item) => {
    return (
      <li className="list-group-item"
        key={item.id}>
        <OrderItem 
          item={item} 
          onInc={()=>inc()}
          onDec={()=>dec()}
          />
      </li>
    )
  })

  return (
    <div>
      <h4>Your Order: {counter}</h4>
      <ul className="item-list list-group">
        {items}
      </ul>
    </div>
  )
};

const mapMethodsToProps = (bookstoreService) => ({ getData: bookstoreService.getOrder });

const mapStateToProps = (state) =>{
  return {
      counter: state
  }
}
const mapDispatchToProps = (dispatch) =>{
  return bindActionCreators(actions,dispatch);    
}

export default connect(mapStateToProps,mapDispatchToProps)(
  withBookstoreService(withData(Order), mapMethodsToProps)
  );