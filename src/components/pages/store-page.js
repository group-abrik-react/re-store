import React from 'react';
import Catalog from '../catalog';
import Order from '../order';

const StorePage = () => {
    return (
        <div>
            <Catalog />
            <Order />
        </div>
    )
}

export default StorePage;