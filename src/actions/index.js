import {inc, dec,
    fetchBooks,
    onIncrease,
    onDecrease,
    onDelete,
    onAddToCart} from './actions';
export {
    inc,
    dec,
    fetchBooks,
    onIncrease,
    onDecrease,
    onDelete,
    onAddToCart
}