export const inc = () => ({ type: 'INC' });
export const dec = () => ({ type: 'DEC' });


const booksRequested = () =>{
    return{
        type: "FETCH_BOOKS_REQUEST"
    }
};

const booksLoaded = (newBooks) => {
    return {
        type: 'FETCH_BOOKS_SUCCESS',
        payload: newBooks
    }
};



const booksError = (error) =>{
    return{
        type: "FETCH_BOOKS_FAILURE",
        payload: error
    }
};

const fetchBooks = (dispatch,bookstoreService)=>()=>{
    dispatch(booksRequested());

    bookstoreService.getBooks()
        .then((data) => dispatch(booksLoaded(data)))
        .catch((err) => dispatch(booksError(err)));
}

const onAddToCart = (bookId) =>{
    return{
        type: 'BOOK_ADD_TO_CART',
        payload: bookId
    }
}


const onIncrease = (bookId) =>{
    return{
        type: 'INCREASE_BOOK',
        payload: bookId
    }
}

const onDecrease = (book) =>{
    return{
        type: 'DECREASE_BOOK',
        payload: book
    }
}

const onDelete = (book) =>{
    return{
        type: 'DELETE_BOOK',
        payload: book
    }
}
export {
    fetchBooks,
    onIncrease,
    onDecrease,
    onDelete,
    onAddToCart
};