import updateBookList from "./reducerBookList";
import updateShopingCart from "./reducerShopingCart";

const reducer = (state, action) => {
    return {
        bookList: updateBookList(state, action),
        shopingCart: updateShopingCart(state, action)
    }
}
export default reducer;