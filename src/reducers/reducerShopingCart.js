const updateItems = (items, item, idx) => {
    if (idx === -1) {
        return [
            ...items,
            item
        ]
    }
    if (item?.count <= 0) {
        return [
            ...items.slice(0, idx),
            ...items.slice(idx + 1)
        ];

    }
    return [
        ...items.slice(0, idx),
        item,
        ...items.slice(idx + 1)
    ]



}
const updateItem = (book, item = {}, quant) => {
    let {
        id = book.id,
        count = 0,
        title = book.title,
        total = 0 } = item;

    if (quant == 0)
        count = 0;
    return {
        id,
        title,
        count: count + 1 * quant,
        total: total + book.price * quant
    };
}

const update = (state, bookId, factor) => {
    const { bookList: { books }, shopingCart: { items } = [] } = state;
    const book = books.find((book) => book.id === bookId);
    const idx = items.findIndex(el => el.id === bookId);
    const item = items[idx];

    let newItem = updateItem(book, item, factor);

    return {
        orderTotal: 0,
        items: updateItems(items, newItem, idx)
    }

}

const updateShopingCart = (state, action) => {
    if (state === undefined) {
        return {
            items: [],
            orderTotal: 0
        }
    }

    switch (action.type) {
        case 'INCREASE_BOOK':
            return update(state, action.payload, 1);

        case 'DECREASE_BOOK':
            return update(state, action.payload, -1);

        case 'DELETE_BOOK':
            return update(state, action.payload, 0);
        default:
            return state.shopingCart;
    }
}
export default updateShopingCart;