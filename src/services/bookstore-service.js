export default class BookstoreService {
    _books = [
        {
            id: 1,
            title: "React быстро. Веб-приложения на React, JSX, Redux и GraphQL",
            autor: "Мардан Азат",
            price: 100,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/41DsrWPebwL._SX298_BO1,204,203,200_.jpg'
        },
        {
            id: 2,
            title: "React и Redux. Функциональная веб-разработка",
            autor: "Бэнкс Алекс, Порселло Ева",
            price: 987,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/514KoNVo3KL._SX357_BO1,204,203,200_.jpg'
        }
    ];
    _order = [
        {
            id: 1,
            title: "React быстро. Веб-приложения на React, JSX, Redux и GraphQL",
            count: 1,
            cost: 100
        },
        {
            id: 2,
            title: "React и Redux. Функциональная веб-разработка",
            count: 2,
            cost: 987
        }
    ]

    getBooks() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(this._books)
                /*if (Math.random() > 0.75) {
                    reject(new Error('Something bad happend'));
                }
                else {
                    resolve(this._books)
                }*/
            }, 700)
        });
    }
}