import { createStore, applyMiddleware } from 'redux';
import reducer from './reducers';


const logMiddelware = ({getState})=>(dispatch) =>(action)=>{
    console.log(action.type, getState(0));
    return dispatch(action);
}

const store = createStore(reducer,applyMiddleware(logMiddelware));

export default store;

